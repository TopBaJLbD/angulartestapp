﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularTestApp.Models;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;

namespace AngularTestApp.Controllers
{
    [Authorize]
    public class PrivateOfficeController:Controller
    {
        public PrivateOfficeController()
        {

        }

        public ActionResult Index()
        {
            return View();
        }
    }
}
